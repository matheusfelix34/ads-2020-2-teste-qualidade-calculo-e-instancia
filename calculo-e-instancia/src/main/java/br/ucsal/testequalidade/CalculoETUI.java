package br.ucsal.testequalidade;

/**
 * O uso de métodos de instância/publicos nesta classe é intencional e objetiva
 * ilustrar o uso de mocks para métodos de instância.
 * 
 * @author claudioneiva
 *
 */
public class CalculoETUI {

	public CalculoEHelper calculoEHelper;
	public TuiUtil tuiUtil;

	public CalculoETUI(CalculoEHelper calculoEHelper, TuiUtil tuiUtil) {
		this.calculoEHelper = calculoEHelper;
		this.tuiUtil = tuiUtil;
	}

	public void obterNCalcularExibirE() {
		Integer n = null;
		Double e;
		String mensagem;

		n = tuiUtil.obterNumeroInteiroPositivo();
		e = calculoEHelper.calcularE(n);
		mensagem = "E(" + n + ")=" + e;
		tuiUtil.exibirMensagem(mensagem);
	}

}
