package br.ucsal.testequalidade;

/**
 * O uso de métodos de instância/publicos nesta classe é intencional e objetiva
 * ilustrar o uso de mocks para métodos de instância.
 * 
 * @author claudioneiva
 *
 */

public class FatorialHelper {

	// Este método possui uma implementação errada. Isto é intensional, para
	// provocar a quebra dos testes integrados, mas não interfere nos testes
	// automatizados exemplificados.
	// A implementação correta é, no lugar de "for (int i = 1; i < x; i++) {"
	// deveria ser "for (int i = 1; i <= x; i++) {".
	public Long calcularFatorial(int x) {
		Long fatorial = 1L;
		for (int i = 1; i < x; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

}
