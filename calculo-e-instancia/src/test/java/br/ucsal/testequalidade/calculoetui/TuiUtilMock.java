package br.ucsal.testequalidade.calculoetui;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import br.ucsal.testequalidade.TuiUtil;

public class TuiUtilMock extends TuiUtil {

	// Map<mensagem, qtd-chamadas>
	private Map<String, Integer> chamadasExibirMensagem = new HashMap<>();

	@Override
	public Integer obterNumeroInteiroPositivo() {
		return 2;
	}

	@Override
	public void exibirMensagem(String mensagem) {
		if (!chamadasExibirMensagem.containsKey(mensagem)) {
			chamadasExibirMensagem.put(mensagem, 0);
		}
		chamadasExibirMensagem.put(mensagem, chamadasExibirMensagem.get(mensagem) + 1);
	}

	/**
	 * Sucesso caso ocorra uma única chamada ao método exibir mensagem para a
	 * mensagem passada como parâmetro.
	 * 
	 * @param mensagem mensagem para a qual será verificada a ocorrência de uma
	 *                 única chamada.
	 */
	public void verificarChamadaExibirMensagem(String mensagem) {
		Assertions.assertEquals(1, chamadasExibirMensagem.get(mensagem));
	}

}
