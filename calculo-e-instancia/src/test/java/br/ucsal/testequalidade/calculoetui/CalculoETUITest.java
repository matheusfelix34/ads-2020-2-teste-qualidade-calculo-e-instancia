package br.ucsal.testequalidade.calculoetui;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.testequalidade.CalculoETUI;

public class CalculoETUITest {

	private static CalculoETUI calculoETUI;
	private static CalculoEHelperStub calculoEHelperStub;
	private static TuiUtilMock tuiUtilMock;

	@BeforeAll
	public static void setup() {
		calculoEHelperStub = new CalculoEHelperStub();
		tuiUtilMock = new TuiUtilMock();
		calculoETUI = new CalculoETUI(calculoEHelperStub, tuiUtilMock);
	}

	@Test
	public void testarOobterNCalcularExibirE2() {
		calculoETUI.obterNCalcularExibirE();
		
		String mensagemEsperada = "E(2)=2.5";
		tuiUtilMock.verificarChamadaExibirMensagem(mensagemEsperada);
	}

}
