package br.ucsal.testequalidade.calculoetui;

import br.ucsal.testequalidade.CalculoEHelper;

public class CalculoEHelperStub extends CalculoEHelper {

	public CalculoEHelperStub() {
		super(null);
	}

	@Override
	public Double calcularE(Integer n) {
		return 2.5;
	}

}
