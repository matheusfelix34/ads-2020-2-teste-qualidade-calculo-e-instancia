package br.ucsal.testequalidade.calculoehelper;

import br.ucsal.testequalidade.FatorialHelper;

public class FatorialHelperStub extends FatorialHelper {

	@Override
	public Long calcularFatorial(int x) {
		switch (x) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		default:
			throw new RuntimeException("calcularFatoria(" + x + ") não definido no FatorialHelperStub");
		}
	}

}
