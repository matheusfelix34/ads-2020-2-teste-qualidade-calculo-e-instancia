package br.ucsal.testequalidade.calculoehelper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.testequalidade.CalculoEHelper;
import br.ucsal.testequalidade.FatorialHelper;

public class CalculoEHelperIntegradoTest {

	private static FatorialHelper fatorialHelper;
	private static CalculoEHelper calculoEHelper;

	@BeforeAll
	public static void setup() {
		fatorialHelper = new FatorialHelper();
		calculoEHelper = new CalculoEHelper(fatorialHelper);
	}

	@Test
	public void calcularE2() {
		Integer n = 2;
		Double eEsperado = 2.5;
		Double eAtual = calculoEHelper.calcularE(n);
		assertEquals(eEsperado, eAtual);
	}

}
