package br.ucsal.testequalidade.fatorialhelper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import br.ucsal.testequalidade.FatorialHelper;

public class FatorialHelperTest {

	private static FatorialHelper fatorialHelper;

	@BeforeAll
	public static void setup() {
		fatorialHelper = new FatorialHelper();
	}

	@Test
	public void testarCalcularFatorial0() {
		int n = 0;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = fatorialHelper.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	public void testarCalcularFatorial5() {
		int n = 5;
		Long fatorialEsperado = 120L;
		Long fatorialAtual = fatorialHelper.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
