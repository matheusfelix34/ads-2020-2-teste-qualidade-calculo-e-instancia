package br.ucsal.testequalidade.unitarioscommockito;

import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.testequalidade.TuiUtil;

public class TuiUtilTest {

	private static Scanner scannerMock;

	private static TuiUtil tuiUtil;

	@BeforeAll
	public static void setup() {
		scannerMock = Mockito.mock(Scanner.class);
		tuiUtil = new TuiUtil();
		TuiUtil.scanner = scannerMock;
	}

	@Test
	public void testarEntradaDoisValoresForaFaixa() {
		// dados de entrada
		Mockito.when(scannerMock.nextInt()).thenReturn(-1).thenReturn(-9).thenReturn(30);

		// saída esperada
		Integer nEsperado = 30;

		// execução do método que desejamos testar e obtenção da saída atual
		Integer nAtual = tuiUtil.obterNumeroInteiroPositivo();

		// comparação entre a saída esperada e a saída atual
		Assertions.assertEquals(nEsperado, nAtual);
	}

}
