package br.ucsal.testequalidade.unitarioscommockito;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.testequalidade.CalculoEHelper;
import br.ucsal.testequalidade.CalculoETUI;
import br.ucsal.testequalidade.TuiUtil;

public class CalculoETUITest {

	private static CalculoETUI calculoETUI;
	private static CalculoEHelper calculoEHelperMock;
	private static TuiUtil tuiUtilMock;

	@BeforeAll
	public static void setup() {

		// Criar o mock.
		calculoEHelperMock = Mockito.mock(CalculoEHelper.class);
		tuiUtilMock = Mockito.mock(TuiUtil.class);

		calculoETUI = new CalculoETUI(calculoEHelperMock, tuiUtilMock);
	}

	@Test
	public void testarOobterNCalcularExibirE2() {
		// Ensinar o mock quais respostas devem ser dadas às chamadas dos métodos.
		Mockito.when(tuiUtilMock.obterNumeroInteiroPositivo()).thenReturn(2);
		Mockito.when(calculoEHelperMock.calcularE(2)).thenReturn(2.5);

		calculoETUI.obterNCalcularExibirE();

		String mensagemEsperada = "E(2)=2.5";
		Mockito.verify(tuiUtilMock).exibirMensagem(mensagemEsperada);
	}

	@Test
	public void testarOobterNCalcularExibirE3() {
		// Ensinar o mock quais respostas devem ser dadas às chamadas dos métodos.
		Mockito.when(tuiUtilMock.obterNumeroInteiroPositivo()).thenReturn(3);
		Mockito.when(calculoEHelperMock.calcularE(3)).thenReturn(2.6666666666666665);

		calculoETUI.obterNCalcularExibirE();

		String mensagemEsperada = "E(3)=2.6666666666666665";
		Mockito.verify(tuiUtilMock).exibirMensagem(mensagemEsperada);
	}

}
